<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Test;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @Route("/test")
 */
class DefaultController extends FOSRestController
{
    
    /**
     * @Rest\Get("/")
     * @Rest\View(
     *  serializerGroups={
     *      "testMinimal"
     *  }
     * )
     * @ApiDoc(
     *  resource=true,
     *  description="Returns a collection of Objects",
     *  section="Tests",
     *  tags={},
     *  output={
     *      "class"="AppBundle\Entity\Test",
     *      "groups"={
     *          "testMinimal"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the test is not found"
     *     }
     * )
     */
    public function getAction()
    {
        $result = $this->get('app.test.service')->getTest();
        return array('item'=>$result);
    }
    /**             
    * @Rest\Get("/{id}")
    * @Rest\View(
    *  serializerGroups={
    *      "testComplete"
    *  }
    * )            
    * @ApiDoc(
    *  resource=true,
    *  description="Returns one Object",
    *  section="Tests",
    *  tags={},
    *  statusCodes={
    *         200="Returned when successful",
    *         404="Returned when the test is not found"
    *     },
    *  output={
    *      "class"="AppBundle\Entity\Test",
    *      "groups"={
    *          "testComplete"
    *      }
    * }
    * )
    */   
    public function getOneAction($id)     
    {      
         $result = $this->get('app.test.service')->getTest($id);
         return $result;
    }
    /**
    * @Rest\Post("/")
    * @Rest\View(
    *  serializerGroups={
    *      "testComplete"
    *  })
    * @ApiDoc(
    *  resource=true,
    *  description="Create a new Object",
    *  input="AppBundle\Form\Type\TestFormType",
    *  section="Tests",
    *  tags={},
    *  statusCodes={
    *         200="Returned when successful",
    *         404="Returned when the test is not found"
    *     },
    *  output={
    *      "class"="AppBundle\Entity\Test",
    *      "groups"={
    *          "testComplete"
    *      }
    * }
    * )
    */   
    public function addAction(Request $request)
    {
        $test = new Test();

        $form = $this->createForm(new \AppBundle\Form\Type\TestFormType(), $test);

        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($test);
            $em->flush();

            return $test;
        }

        return View::create($form, 400);
    }
    /**
    * @Rest\Put("/{id}")
    * @Rest\View(
    *  serializerGroups={
    *      "testComplete"
    *  })
    * @ApiDoc(
    *  resource=true,
    *  description="Patch Object",
    *  input="AppBundle\Form\Type\TestFormType",
    *  section="Tests",
    *  tags={},
    *  statusCodes={
    *         200="Returned when successful",
    *         404="Returned when the test is not found"
    *     },
    *  output={
    *      "class"="AppBundle\Entity\Test",
    *      "groups"={
    *          "testComplete"
    *      }
    * })
    */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $test = $em->getRepository('AppBundle:Test')->find($id);

        $form = $this->createForm(new \AppBundle\Form\Type\TestFormType(), $test, array('method' => 'PUT'));

        $form->handleRequest($request);
        if ($test) {
            if ($form->isValid()) {

                $em->persist($test);
                $em->flush();

                return $test;
            } else {
                return View::create($form, 400);
            }
        } else {
            throw $this->createNotFoundException('Not found!');
        }
    }
    /**
    * @Rest\Delete("/{id}")
    * @ApiDoc(
    *  resource=true,
    *  description="Remove Object",
    *  section="Tests",
    *  tags={},
    *  statusCodes={
    *         200="Returned when successful",
    *         404="Returned when the test is not found"
    *     }
    * )
    */
    public function deleteAction($id)
    {
        $result = $this->get('app.test.service')->deleteTest($id);
        return $result;    
        
    }

}
