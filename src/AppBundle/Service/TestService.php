<?php

namespace AppBundle\Service;

use AppBundle\Entity\Test;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TestService
{
    private $em;
    
    public function __construct(EntityManager $em){
        $this->em = $em;
    }
    
    public function getTest($id = null){
        
        if(is_null($id)){
            $result = $this->em->getRepository('AppBundle:Test')->findAll();
        } else {
            $result = $this->findOneTest($id);
        }
         
        return $result;
    }
    
    
    public function deleteTest($id){
        
        $test = $this->findOneTest($id);
        
        if ($test) {
                $this->em->remove($test);
                $this->em->flush();
                return array('message'=>'Deleted');

        } else {
            throw new NotFoundHttpException("Not found");
        }
    }
    
    private function findOneTest($id){
        $result = $this->em->getRepository('AppBundle:Test')->find($id);
        if(!$result){
            throw new NotFoundHttpException("Not found");
        }
        return $result;
    }
}