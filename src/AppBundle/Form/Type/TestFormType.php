<?php  

namespace AppBundle\Form\Type;  
 
use Symfony\Component\Form\AbstractType;  
use Symfony\Component\Form\FormBuilderInterface;  
use Symfony\Component\OptionsResolver\OptionsResolver;  
 
class TestFormType extends AbstractType  
{      
public function buildForm(FormBuilderInterface $builder, array $options)     {                 
         $builder
             ->add('name')
             ->add('body')
        ;
    }
 
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Test',
            'csrf_protection'   => false
        ));
    }
 
 
    public function getName()
    {
        return 'test';
    }
}